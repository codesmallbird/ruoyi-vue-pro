package cn.iocoder.yudao.module.pet.service.pet;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.annotation.Resource;

import cn.iocoder.yudao.framework.test.core.ut.BaseDbUnitTest;

import cn.iocoder.yudao.module.pet.controller.app.pet.vo.*;
import cn.iocoder.yudao.module.pet.dal.dataobject.pet.PetDO;
import cn.iocoder.yudao.module.pet.dal.mysql.pet.PetMapper;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import javax.annotation.Resource;
import org.springframework.context.annotation.Import;
import java.util.*;
import java.time.LocalDateTime;

import static cn.hutool.core.util.RandomUtil.*;
import static cn.iocoder.yudao.module.pet.enums.ErrorCodeConstants.*;
import static cn.iocoder.yudao.framework.test.core.util.AssertUtils.*;
import static cn.iocoder.yudao.framework.test.core.util.RandomUtils.*;
import static cn.iocoder.yudao.framework.common.util.object.ObjectUtils.*;
import static cn.iocoder.yudao.framework.common.util.date.DateUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
* {@link PetServiceImpl} 的单元测试类
*
* @author 芋道源码
*/
@Import(PetServiceImpl.class)
public class PetServiceImplTest extends BaseDbUnitTest {

    @Resource
    private PetServiceImpl Service;

    @Resource
    private PetMapper Mapper;

    @Test
    public void testCreate_success() {
        // 准备参数
        AppPetCreateReqVO reqVO = randomPojo(AppPetCreateReqVO.class);

        // 调用
        Long Id = Service.create(reqVO);
        // 断言
        assertNotNull(Id);
        // 校验记录的属性是否正确
        PetDO petDO = Mapper.selectById(Id);
        assertPojoEquals(reqVO,petDO );
    }

    @Test
    public void testUpdate_success() {
        // mock 数据
        PetDO db = randomPojo(PetDO.class);
        Mapper.insert(db);// @Sql: 先插入出一条存在的数据
        // 准备参数
        AppPetUpdateReqVO reqVO = randomPojo(AppPetUpdateReqVO.class, o -> {
            o.setId(db.getId()); // 设置更新的 ID
        });

        // 调用
        Service.update(reqVO);
        // 校验是否更新正确
        PetDO petDO = Mapper.selectById(reqVO.getId()); // 获取最新的
        assertPojoEquals(reqVO, petDO);
    }

    @Test
    public void testUpdate_notExists() {
        // 准备参数
        AppPetUpdateReqVO reqVO = randomPojo(AppPetUpdateReqVO.class);

        // 调用, 并断言异常
        assertServiceException(() -> Service.update(reqVO), _NOT_EXISTS);
    }

    @Test
    public void testDelete_success() {
        // mock 数据
        PetDO db = randomPojo(PetDO.class);
        Mapper.insert(db);// @Sql: 先插入出一条存在的数据
        // 准备参数
        Long id = db.getId();

        // 调用
        Service.delete(id);
       // 校验数据不存在了
       assertNull(Mapper.selectById(id));
    }

    @Test
    public void testDelete_notExists() {
        // 准备参数
        Long id = randomLongId();

        // 调用, 并断言异常
        assertServiceException(() -> Service.delete(id), _NOT_EXISTS);
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetPage() {
       // mock 数据
       PetDO db = randomPojo(PetDO.class, o -> { // 等会查询到
           o.setPostType(null);
           o.setPetType(null);
           o.setPetName(null);
           o.setPetSex(null);
           o.setProvince(null);
           o.setCity(null);
           o.setCounty(null);
           o.setAddress(null);
           o.setExpireTime(null);
           o.setContact(null);
           o.setPhone(null);
           o.setDetail(null);
           o.setImgs(null);
           o.setIsFree(null);
           o.setStatus(null);
           o.setVerifyStatus(null);
           o.setUserIp(null);
           o.setCreateTime(null);
       });
       Mapper.insert(db);
       // 测试 postType 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPostType(null)));
       // 测试 petType 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPetType(null)));
       // 测试 petName 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPetName(null)));
       // 测试 petSex 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPetSex(null)));
       // 测试 province 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setProvince(null)));
       // 测试 city 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setCity(null)));
       // 测试 county 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setCounty(null)));
       // 测试 address 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setAddress(null)));
       // 测试 expireTime 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setExpireTime(null)));
       // 测试 contact 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setContact(null)));
       // 测试 phone 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPhone(null)));
       // 测试 detail 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setDetail(null)));
       // 测试 imgs 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setImgs(null)));
       // 测试 isFree 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setIsFree(null)));
       // 测试 status 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setStatus(null)));
       // 测试 verifyStatus 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setVerifyStatus(null)));
       // 测试 userIp 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setUserIp(null)));
       // 测试 createTime 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setCreateTime(null)));
       // 准备参数
       AppPetPageReqVO reqVO = new AppPetPageReqVO();
       reqVO.setPostType(null);
       reqVO.setPetType(null);
       reqVO.setPetName(null);
       reqVO.setPetSex(null);
       reqVO.setProvince(null);
       reqVO.setCity(null);
       reqVO.setCounty(null);
       reqVO.setAddress(null);
       reqVO.setExpireTime((new LocalDateTime[]{}));
       reqVO.setContact(null);
       reqVO.setPhone(null);
       reqVO.setDetail(null);
       reqVO.setImgs(null);
       reqVO.setIsFree(null);
       reqVO.setStatus(null);
       reqVO.setVerifyStatus(null);
       reqVO.setUserIp(null);
       reqVO.setCreateTime((new LocalDateTime[]{}));

       // 调用
       PageResult<PetDO> pageResult = Service.getPage(reqVO);
       // 断言
       assertEquals(1, pageResult.getTotal());
       assertEquals(1, pageResult.getList().size());
       assertPojoEquals(db, pageResult.getList().get(0));
    }

    @Test
    @Disabled  // TODO 请修改 null 为需要的值，然后删除 @Disabled 注解
    public void testGetList() {
       // mock 数据
       PetDO db = randomPojo(PetDO.class, o -> { // 等会查询到
           o.setPostType(null);
           o.setPetType(null);
           o.setPetName(null);
           o.setPetSex(null);
           o.setProvince(null);
           o.setCity(null);
           o.setCounty(null);
           o.setAddress(null);
           o.setExpireTime(null);
           o.setContact(null);
           o.setPhone(null);
           o.setDetail(null);
           o.setImgs(null);
           o.setIsFree(null);
           o.setStatus(null);
           o.setVerifyStatus(null);
           o.setUserIp(null);
           o.setCreateTime(null);
       });
       Mapper.insert(db);
       // 测试 postType 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPostType(null)));
       // 测试 petType 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPetType(null)));
       // 测试 petName 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPetName(null)));
       // 测试 petSex 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPetSex(null)));
       // 测试 province 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setProvince(null)));
       // 测试 city 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setCity(null)));
       // 测试 county 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setCounty(null)));
       // 测试 address 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setAddress(null)));
       // 测试 expireTime 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setExpireTime(null)));
       // 测试 contact 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setContact(null)));
       // 测试 phone 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setPhone(null)));
       // 测试 detail 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setDetail(null)));
       // 测试 imgs 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setImgs(null)));
       // 测试 isFree 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setIsFree(null)));
       // 测试 status 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setStatus(null)));
       // 测试 verifyStatus 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setVerifyStatus(null)));
       // 测试 userIp 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setUserIp(null)));
       // 测试 createTime 不匹配
       Mapper.insert(cloneIgnoreId(db, o -> o.setCreateTime(null)));
       // 准备参数
       AppPetExportReqVO reqVO = new AppPetExportReqVO();
       reqVO.setPostType(null);
       reqVO.setPetType(null);
       reqVO.setPetName(null);
       reqVO.setPetSex(null);
       reqVO.setProvince(null);
       reqVO.setCity(null);
       reqVO.setCounty(null);
       reqVO.setAddress(null);
       reqVO.setExpireTime((new LocalDateTime[]{}));
       reqVO.setContact(null);
       reqVO.setPhone(null);
       reqVO.setDetail(null);
       reqVO.setImgs(null);
       reqVO.setIsFree(null);
       reqVO.setStatus(null);
       reqVO.setVerifyStatus(null);
       reqVO.setUserIp(null);
       reqVO.setCreateTime((new LocalDateTime[]{}));

       // 调用
       List<PetDO> list = Service.getList(reqVO);
       // 断言
       assertEquals(1, list.size());
       assertPojoEquals(db, list.get(0));
    }

}
