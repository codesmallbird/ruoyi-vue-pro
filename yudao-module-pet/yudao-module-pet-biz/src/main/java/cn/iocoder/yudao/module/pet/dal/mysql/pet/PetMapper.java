package cn.iocoder.yudao.module.pet.dal.mysql.pet;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.mybatis.core.query.LambdaQueryWrapperX;
import cn.iocoder.yudao.framework.mybatis.core.mapper.BaseMapperX;
import cn.iocoder.yudao.module.pet.dal.dataobject.pet.PetDO;
import org.apache.ibatis.annotations.Mapper;
import cn.iocoder.yudao.module.pet.controller.app.pet.vo.*;

/**
 * 宠物 Mapper
 *
 * @author 芋道源码
 */
@Mapper
public interface PetMapper extends BaseMapperX<PetDO> {

    default PageResult<PetDO> selectPage(AppPetPageReqVO reqVO) {
        return selectPage(reqVO, new LambdaQueryWrapperX<PetDO>()
                .eqIfPresent(PetDO::getPostType, reqVO.getPostType())
                .eqIfPresent(PetDO::getPetType, reqVO.getPetType())
                .eqIfPresent(PetDO::getUserId, reqVO.getUserId())
                .likeIfPresent(PetDO::getPetName, reqVO.getPetName())
                .eqIfPresent(PetDO::getPetSex, reqVO.getPetSex())
                .eqIfPresent(PetDO::getProvince, reqVO.getProvince())
                .eqIfPresent(PetDO::getCity, reqVO.getCity())
                .eqIfPresent(PetDO::getCounty, reqVO.getCounty())
                .eqIfPresent(PetDO::getAddress, reqVO.getAddress())
                .betweenIfPresent(PetDO::getExpireTime, reqVO.getExpireTime())
                .eqIfPresent(PetDO::getContact, reqVO.getContact())
                .eqIfPresent(PetDO::getPhone, reqVO.getPhone())
                .eqIfPresent(PetDO::getDetail, reqVO.getDetail())
                .eqIfPresent(PetDO::getImgs, reqVO.getImgs())
                .eqIfPresent(PetDO::getIsFree, reqVO.getIsFree())
                .eqIfPresent(PetDO::getStatus, reqVO.getStatus())
                .eqIfPresent(PetDO::getVerifyStatus, reqVO.getVerifyStatus())
                .eqIfPresent(PetDO::getUserIp, reqVO.getUserIp())
                .betweenIfPresent(PetDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(PetDO::getId));
    }

    default List<PetDO> selectList(AppPetExportReqVO reqVO) {
        return selectList(new LambdaQueryWrapperX<PetDO>()
                .eqIfPresent(PetDO::getPostType, reqVO.getPostType())
                .eqIfPresent(PetDO::getPetType, reqVO.getPetType())
                .eqIfPresent(PetDO::getUserId, reqVO.getUserId())
                .likeIfPresent(PetDO::getPetName, reqVO.getPetName())
                .eqIfPresent(PetDO::getPetSex, reqVO.getPetSex())
                .eqIfPresent(PetDO::getProvince, reqVO.getProvince())
                .eqIfPresent(PetDO::getCity, reqVO.getCity())
                .eqIfPresent(PetDO::getCounty, reqVO.getCounty())
                .eqIfPresent(PetDO::getAddress, reqVO.getAddress())
                .betweenIfPresent(PetDO::getExpireTime, reqVO.getExpireTime())
                .eqIfPresent(PetDO::getContact, reqVO.getContact())
                .eqIfPresent(PetDO::getPhone, reqVO.getPhone())
                .eqIfPresent(PetDO::getDetail, reqVO.getDetail())
                .eqIfPresent(PetDO::getImgs, reqVO.getImgs())
                .eqIfPresent(PetDO::getIsFree, reqVO.getIsFree())
                .eqIfPresent(PetDO::getStatus, reqVO.getStatus())
                .eqIfPresent(PetDO::getVerifyStatus, reqVO.getVerifyStatus())
                .eqIfPresent(PetDO::getUserIp, reqVO.getUserIp())
                .betweenIfPresent(PetDO::getCreateTime, reqVO.getCreateTime())
                .orderByDesc(PetDO::getId));
    }

}
