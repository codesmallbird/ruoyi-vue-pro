package cn.iocoder.yudao.module.pet.convert.pet;

import java.util.*;

import cn.iocoder.yudao.framework.common.pojo.PageResult;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import cn.iocoder.yudao.module.pet.controller.app.pet.vo.*;
import cn.iocoder.yudao.module.pet.dal.dataobject.pet.PetDO;

/**
 * 宠物 Convert
 *
 * @author 芋道源码
 */
@Mapper
public interface PetConvert {

    PetConvert INSTANCE = Mappers.getMapper(PetConvert.class);

    PetDO convert(AppPetCreateReqVO bean);

    PetDO convert(AppPetUpdateReqVO bean);

    AppPetRespVO convert(PetDO bean);

    List<AppPetRespVO> convertList(List<PetDO> list);

    PageResult<AppPetRespVO> convertPage(PageResult<PetDO> page);

    List<AppPetExcelVO> convertList02(List<PetDO> list);

}
