package cn.iocoder.yudao.module.pet.controller.app.pet;

import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.*;

import javax.validation.constraints.*;
import javax.validation.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;

import cn.iocoder.yudao.framework.common.pojo.PageResult;
import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;

import cn.iocoder.yudao.framework.excel.core.util.ExcelUtils;

import cn.iocoder.yudao.framework.operatelog.core.annotations.OperateLog;
import static cn.iocoder.yudao.framework.operatelog.core.enums.OperateTypeEnum.*;

import cn.iocoder.yudao.module.pet.controller.app.pet.vo.*;
import cn.iocoder.yudao.module.pet.dal.dataobject.pet.PetDO;
import cn.iocoder.yudao.module.pet.convert.pet.PetConvert;
import cn.iocoder.yudao.module.pet.service.pet.PetService;

@Api(tags = "用户 APP - 宠物")
@RestController
@RequestMapping("/pet/")
@Validated
public class AppPetController {

    @Resource
    private PetService Service;

    @PostMapping("/create")
    @ApiOperation("创建宠物")

    public CommonResult<Long> create(@Valid @RequestBody AppPetCreateReqVO createReqVO) {
        return success(Service.create(createReqVO));
    }

    @PutMapping("/update")
    @ApiOperation("更新宠物")

    public CommonResult<Boolean> update(@Valid @RequestBody AppPetUpdateReqVO updateReqVO) {
        Service.update(updateReqVO);
        return success(true);
    }

    @DeleteMapping("/delete")
    @ApiOperation("删除宠物")
    @ApiImplicitParam(name = "id", value = "编号", required = true, dataTypeClass = Long.class)

    public CommonResult<Boolean> delete(@RequestParam("id") Long id) {
        Service.delete(id);
        return success(true);
    }

    @GetMapping("/get")
    @ApiOperation("获得宠物")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024", dataTypeClass = Long.class)

    public CommonResult<AppPetRespVO> get(@RequestParam("id") Long id) {
        PetDO petDO = Service.get(id);
        return success(PetConvert.INSTANCE.convert(petDO));
    }

    @GetMapping("/list")
    @ApiOperation("获得宠物列表")
    @ApiImplicitParam(name = "ids", value = "编号列表", required = true, example = "1024,2048", dataTypeClass = List.class)

    public CommonResult<List<AppPetRespVO>> getList(@RequestParam("ids") Collection<Long> ids) {
        List<PetDO> list = Service.getList(ids);
        return success(PetConvert.INSTANCE.convertList(list));
    }

    @GetMapping("/page")
    @ApiOperation("获得宠物分页")

    public CommonResult<PageResult<AppPetRespVO>> getPage(@Valid AppPetPageReqVO pageVO) {
        PageResult<PetDO> pageResult = Service.getPage(pageVO);
        return success(PetConvert.INSTANCE.convertPage(pageResult));
    }

    @GetMapping("/export-excel")
    @ApiOperation("导出宠物 Excel")

    @OperateLog(type = EXPORT)
    public void exportExcel(@Valid AppPetExportReqVO exportReqVO,
              HttpServletResponse response) throws IOException {
        List<PetDO> list = Service.getList(exportReqVO);
        // 导出 Excel
        List<AppPetExcelVO> datas = PetConvert.INSTANCE.convertList02(list);
        ExcelUtils.write(response, "宠物.xls", "数据", AppPetExcelVO.class, datas);
    }

}
