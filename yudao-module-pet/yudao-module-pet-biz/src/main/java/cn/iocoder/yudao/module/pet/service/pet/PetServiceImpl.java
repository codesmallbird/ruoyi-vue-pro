package cn.iocoder.yudao.module.pet.service.pet;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import org.springframework.validation.annotation.Validated;

import java.util.*;
import cn.iocoder.yudao.module.pet.controller.app.pet.vo.*;
import cn.iocoder.yudao.module.pet.dal.dataobject.pet.PetDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

import cn.iocoder.yudao.module.pet.convert.pet.PetConvert;
import cn.iocoder.yudao.module.pet.dal.mysql.pet.PetMapper;

import static cn.iocoder.yudao.framework.common.exception.util.ServiceExceptionUtil.exception;
import static cn.iocoder.yudao.module.pet.enums.ErrorCodeConstants.*;

/**
 * 宠物 Service 实现类
 *
 * @author 芋道源码
 */
@Service
@Validated
public class PetServiceImpl implements PetService {

    @Resource
    private PetMapper Mapper;

    @Override
    public Long create(AppPetCreateReqVO createReqVO) {
        // 插入
        PetDO  petDO = PetConvert.INSTANCE.convert(createReqVO);
        Mapper.insert(petDO);
        // 返回
        return petDO.getId();
    }

    @Override
    public void update(AppPetUpdateReqVO updateReqVO) {
        // 校验存在
        this.validateExists(updateReqVO.getId());
        // 更新
        PetDO updateObj = PetConvert.INSTANCE.convert(updateReqVO);
        Mapper.updateById(updateObj);
    }

    @Override
    public void delete(Long id) {
        // 校验存在
        this.validateExists(id);
        // 删除
        Mapper.deleteById(id);
    }

    private void validateExists(Long id) {
        if (Mapper.selectById(id) == null) {
            throw exception(_NOT_EXISTS);
        }
    }

    @Override
    public PetDO get(Long id) {
        return Mapper.selectById(id);
    }

    @Override
    public List<PetDO> getList(Collection<Long> ids) {
        return Mapper.selectBatchIds(ids);
    }

    @Override
    public PageResult<PetDO> getPage(AppPetPageReqVO pageReqVO) {
        return Mapper.selectPage(pageReqVO);
    }

    @Override
    public List<PetDO> getList(AppPetExportReqVO exportReqVO) {
        return Mapper.selectList(exportReqVO);
    }

}
