package cn.iocoder.yudao.module.pet.controller.app.pet.vo;

import lombok.*;
import java.util.*;
import io.swagger.annotations.*;
import javax.validation.constraints.*;

@ApiModel("用户 APP - 宠物更新 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppPetUpdateReqVO extends AppPetBaseVO {

    @ApiModelProperty(value = "主键id", required = true, example = "32485")
    @NotNull(message = "主键id不能为空")
    private Long id;

}
