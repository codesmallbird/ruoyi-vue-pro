package cn.iocoder.yudao.module.pet.controller.app.pet.vo;

import lombok.*;
import java.util.*;
import io.swagger.annotations.*;
import javax.validation.constraints.*;

@ApiModel("用户 APP - 宠物创建 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppPetCreateReqVO extends AppPetBaseVO {

}
