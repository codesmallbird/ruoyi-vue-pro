package cn.iocoder.yudao.module.pet.service.pet;

import java.util.*;
import javax.validation.*;
import cn.iocoder.yudao.module.pet.controller.app.pet.vo.*;
import cn.iocoder.yudao.module.pet.dal.dataobject.pet.PetDO;
import cn.iocoder.yudao.framework.common.pojo.PageResult;

/**
 * 宠物 Service 接口
 *
 * @author 芋道源码
 */
public interface PetService {

    /**
     * 创建宠物
     *
     * @param createReqVO 创建信息
     * @return 编号
     */
    Long create(@Valid AppPetCreateReqVO createReqVO);

    /**
     * 更新宠物
     *
     * @param updateReqVO 更新信息
     */
    void update(@Valid AppPetUpdateReqVO updateReqVO);

    /**
     * 删除宠物
     *
     * @param id 编号
     */
    void delete(Long id);

    /**
     * 获得宠物
     *
     * @param id 编号
     * @return 宠物
     */
    PetDO get(Long id);

    /**
     * 获得宠物列表
     *
     * @param ids 编号
     * @return 宠物列表
     */
    List<PetDO> getList(Collection<Long> ids);

    /**
     * 获得宠物分页
     *
     * @param pageReqVO 分页查询
     * @return 宠物分页
     */
    PageResult<PetDO> getPage(AppPetPageReqVO pageReqVO);

    /**
     * 获得宠物列表, 用于 Excel 导出
     *
     * @param exportReqVO 查询条件
     * @return 宠物列表
     */
    List<PetDO> getList(AppPetExportReqVO exportReqVO);

}
