package cn.iocoder.yudao.module.pet.controller.app.pet.vo;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import io.swagger.annotations.*;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 宠物 Excel VO
 *
 * @author 芋道源码
 */
@Data
public class AppPetExcelVO {

    @ExcelProperty("主键id")
    private Long id;

    @ExcelProperty("帖子类型 1 送养 2 丢失 3 捡到 4发现")
    private Integer postType;

    @ExcelProperty("宠物类型")
    private String petType;

    @ExcelProperty("宠物名称")
    private String petName;

    @ExcelProperty("宠物性别")
    private String petSex;

    @ExcelProperty("省份")
    private String province;

    @ExcelProperty("城市")
    private String city;

    @ExcelProperty("区县")
    private String county;

    @ExcelProperty("详细地址")
    private String address;

    @ExcelProperty("失效时间")
    private LocalDateTime expireTime;

    @ExcelProperty("联系人")
    private String contact;

    @ExcelProperty("联系电话")
    private String phone;

    @ExcelProperty("详细信息")
    private String detail;

    @ExcelProperty("图片列表")
    private String imgs;

    @ExcelProperty("状态 0 免疫 1 无偿")
    private Integer isFree;

    @ExcelProperty("状态 0 未解决 1 解决")
    private Integer status;

    @ExcelProperty("审核状态 0 未审核 1 审核通过 2 审核失败 ")
    private Integer verifyStatus;

    @ExcelProperty("用户 IP")
    private String userIp;

    @ExcelProperty("创建时间")
    private LocalDateTime createTime;

}
