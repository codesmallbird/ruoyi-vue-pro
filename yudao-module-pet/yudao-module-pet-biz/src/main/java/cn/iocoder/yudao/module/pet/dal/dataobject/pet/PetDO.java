package cn.iocoder.yudao.module.pet.dal.dataobject.pet;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.*;
import cn.iocoder.yudao.framework.mybatis.core.dataobject.BaseDO;

/**
 * 宠物 DO
 *
 * @author 芋道源码
 */
@TableName("tb_pet")
@KeySequence("tb_pet_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PetDO extends BaseDO {

    /**
     * 主键id
     */
    @TableId
    private Long id;
    /**
     * 帖子类型 1 送养 2 丢失 3 捡到 4发现
     */
    private Integer postType;
    /**
     * 宠物类型
     */
    private String petType;
    /**
     * 宠物名称
     */
    private String petName;
    /**
     * 宠物性别
     */
    private String petSex;
    /**
     * 省份
     */
    private String province;
    /**
     * 城市
     */
    private String city;
    /**
     * 区县
     */
    private String county;
    /**
     * 详细地址
     */
    private String address;
    /**
     * 失效时间
     */
    private LocalDateTime expireTime;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 详细信息
     */
    private String detail;
    /**
     * 图片列表
     */
    private String imgs;
    /**
     * 状态 0 免疫 1 无偿
     */
    private Integer isFree;
    /**
     * 状态 0 未解决 1 解决
     */
    private Integer status;
    /**
     * 审核状态 0 未审核 1 审核通过 2 审核失败 
     */
    private Integer verifyStatus;
    /**
     * 用户 IP
     */
    private String userIp;

    /**
     * 用户 IP
     */
    private String userId;
}
