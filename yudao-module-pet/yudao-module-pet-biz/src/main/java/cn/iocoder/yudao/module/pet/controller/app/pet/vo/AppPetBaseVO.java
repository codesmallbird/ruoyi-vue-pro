package cn.iocoder.yudao.module.pet.controller.app.pet.vo;

import lombok.*;
import java.util.*;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import java.time.LocalDateTime;
import io.swagger.annotations.*;
import javax.validation.constraints.*;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.iocoder.yudao.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

/**
* 宠物 Base VO，提供给添加、修改、详细的子 VO 使用
* 如果子 VO 存在差异的字段，请不要添加到这里，影响 Swagger 文档生成
*/
@Data
public class AppPetBaseVO {

    @ApiModelProperty(value = "帖子类型 1 送养 2 丢失 3 捡到 4发现", required = true, example = "2")
    @NotNull(message = "帖子类型 1 送养 2 丢失 3 捡到 4发现不能为空")
    private Integer postType;

    @ApiModelProperty(value = "宠物类型", required = true, example = "2")
    @NotNull(message = "宠物类型不能为空")
    private String petType;

    @ApiModelProperty(value = "宠物名称", example = "李四")
    private String petName;

    @ApiModelProperty(value = "宠物性别", required = true)
    @NotNull(message = "宠物性别不能为空")
    private String petSex;

    @ApiModelProperty(value = "省份", required = true)
    @NotNull(message = "省份不能为空")
    private String province;

    @ApiModelProperty(value = "城市", required = true)
    @NotNull(message = "城市不能为空")
    private String city;

    @ApiModelProperty(value = "区县", required = true)
    @NotNull(message = "区县不能为空")
    private String county;

    @ApiModelProperty(value = "详细地址", required = true)
    @NotNull(message = "详细地址不能为空")
    private String address;

    @ApiModelProperty(value = "失效时间", required = true)
    @NotNull(message = "失效时间不能为空")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime expireTime;

    @ApiModelProperty(value = "联系人", required = true)
    @NotNull(message = "联系人不能为空")
    private String contact;

    @ApiModelProperty(value = "联系电话", required = true)
    @NotNull(message = "联系电话不能为空")
    private String phone;

    @ApiModelProperty(value = "详细信息")
    private String detail;

    @ApiModelProperty(value = "图片列表")
    private String imgs;

    @ApiModelProperty(value = "状态 0 免疫 1 无偿", required = true)
    @NotNull(message = "状态 0 免疫 1 无偿不能为空")
    private Integer isFree;

    @ApiModelProperty(value = "状态 0 未解决 1 解决", required = true, example = "2")
    @NotNull(message = "状态 0 未解决 1 解决不能为空")
    private Integer status;

    @ApiModelProperty(value = "审核状态 0 未审核 1 审核通过 2 审核失败 ", required = true, example = "1")
    @NotNull(message = "审核状态 0 未审核 1 审核通过 2 审核失败 不能为空")
    private Integer verifyStatus;

    @ApiModelProperty(value = "用户 IP", required = true)
    @NotNull(message = "用户 IP不能为空")
    private String userIp;

}
