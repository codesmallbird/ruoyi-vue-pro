package cn.iocoder.yudao.module.pet.controller.app.pet.vo;

import lombok.*;
import java.util.*;
import io.swagger.annotations.*;
import cn.iocoder.yudao.framework.common.pojo.PageParam;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDateTime;

import static cn.iocoder.yudao.framework.common.util.date.DateUtils.FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND;

@ApiModel("用户 APP - 宠物分页 Request VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppPetPageReqVO extends PageParam {

    @ApiModelProperty(value = "帖子类型 1 送养 2 丢失 3 捡到 4发现", example = "2")
    private Integer postType;

    @ApiModelProperty(value = "宠物类型", example = "2")
    private String petType;

    @ApiModelProperty(value = "宠物名称", example = "李四")
    private String petName;

    @ApiModelProperty(value = "宠物性别")
    private String petSex;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "区县")
    private String county;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "失效时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] expireTime;

    @ApiModelProperty(value = "联系人")
    private String contact;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "详细信息")
    private String detail;

    @ApiModelProperty(value = "图片列表")
    private String imgs;

    @ApiModelProperty(value = "状态 0 免疫 1 无偿")
    private Integer isFree;

    @ApiModelProperty(value = "状态 0 未解决 1 解决", example = "2")
    private Integer status;

    @ApiModelProperty(value = "审核状态 0 未审核 1 审核通过 2 审核失败 ", example = "1")
    private Integer verifyStatus;

    @ApiModelProperty(value = "用户 IP")
    private String userIp;

    @ApiModelProperty(value = "用户 IP")
    private String userId;

    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern = FORMAT_YEAR_MONTH_DAY_HOUR_MINUTE_SECOND)
    private LocalDateTime[] createTime;

}
