package cn.iocoder.yudao.module.pet.controller.app.pet.vo;

import lombok.*;
import java.time.LocalDateTime;
import io.swagger.annotations.*;

@ApiModel("用户 APP - 宠物 Response VO")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class AppPetRespVO extends AppPetBaseVO {

    @ApiModelProperty(value = "主键id", required = true, example = "32485")
    private Long id;

    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createTime;

}
