package cn.iocoder.yudao.module.pet.enums;

import cn.iocoder.yudao.framework.common.exception.ErrorCode;

/**
 * ${DESCRIPTION}
 *
 * @author wei.zhang
 * @version 1.0
 * @date 2023-01-30 22:56
 */
public interface ErrorCodeConstants {

    ErrorCode _NOT_EXISTS = new ErrorCode(20000001, "宠物不存在");
}
